# ai2html (NatGeo version)

This Illustartor script, originally created by The New York Times' Archie Tse, converts your Illustrator boards in HTML code (for text) and background PNGs (for visual content).

## Installing the Script
- Download the script (ai2html.jsx) and save it at `Applications/Adobe Illustrator CS6/Presets/en_US/Scripts`

## Creating the graphic
- The Illustrator file included in this repository (`Illustrator-template.ai`) features two artboards, one is 1020px wide (medium size), the second one is 570px wide (column width) and the other one 288px (320px, which is the phone size, minus the margins).
- Create your two versions of your graphic/map on each artboard (don't change Artboard names, please), height can be changed.
- If you don't need the 1020px wide artboard, just remove the Artboard in Illustartor
- Be sure your Document Color Mode is set to RGB, and that the elements you create are RGB too (no tints, swatches, etc....)
- Use Arial for sans (it will be automatically converted to Pragmatica) and Georgia for serif (will be converted to Chronicle). Sizes muy slightly vary once converted.
- Once finished, in Illustrator, go to `File/Scripts/ai2html` and click. A message will warn you of any errors or completed processes.
- The script will generate a folder inside the folder where the graphic is saved called ai2thml-output, that will include two PNGs and one html file.
- If you change the name of the file, change the name in the CSS code included just above the Artboards (by default, 'Illustrator-template')

## Adapting the graphic to AEM
- Upload the two images to AEM, activate them, and retrieve the URL (remember to change the url from aem to news)
- In the HTML code you'll get, there's a call to two/three different images (depoending the number of Artboards), the bcolumn-size one in Artboard 1, and the small one in the Artboard 2. The Medium size will be Artboard 3, if needed. You will need top change the relative calls to the URLs you got for the images in AEM. The calls will look like this:

	`<img id='g-ai0-0'
		class='g-aiImg'
		src='Illustrator-template-Artboard_1.png'
	/>`

- So replace `Illustrator-template-Artboard_1.png` with your URL
- If you're using map fonts (not just Pragmatica/Helvetica or Chrnicle/Georgia) you'll need to include the font'faces for those. Create two style tage (`<style></style>`) on the top part of your html file. Look for the font-face you need [here](https://bitbucket.org/snippets/nggraphics/LL4oy/map-fonts). For instance, if you need to use Bumstead, yopur code will look like this:
	`<style>
		@font-face {
			  font-family: 'Bumstead';
			  src: url('http://fonts.ngeo.com/maps/1-0-0/bumstead.woff2') format('woff2'),
			       url('http://fonts.ngeo.com/maps/1-0-0/bumstead.woff') format('woff');
			  format('woff');
			}
	</style>`

## Publishing the graphic
- Upload the HTML to a Interactive component in an AEM article

## Houston, we have a problem
We're still working on making this system work and there are still many many issues to solve. If you find something that's not working, feel free to ask, or check if the issue is being checked [here](https://bitbucket.org/nggraphics/ai2html/issues?status=new&status=open). If it's not, create your own or let us know!